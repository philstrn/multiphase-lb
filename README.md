This is an old code from my PhD. It utilizes the Color-Gradient LB model to simulate a two-phase flow in a between-two-plates channel (I think). I haven't touched this since publishing the paper (2019), so I cannot guarantee it being bug-free.
It can be compiled with:
```
nvcc d3q19_CG_clean_kontra.cu
```
