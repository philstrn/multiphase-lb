#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <cmath>
#include <fstream>
#include <ctime>
#include <chrono>

//1507 332

#define LZ       100
#define LY       100
#define LX       100
#define S         19
#define pfeq   100
#define tmax 10000001
#define fact       1
#define no_sub     2

using namespace std;

__host__ void GeometryFromFile(bool *ActWall, const int x, const int y, const int z);

__global__ void Init(float *f, float *f_new, float *rho, float *U, float *V, float *W, float *Fx, float *Fy, float *Fz, float *wf, int *ex, int *ey, int *ez, const bool *wall, const int x, const int y, const int z);
__host__ void RandomDensity(float *rho, const int x, const int y, const int z);

__global__ void CollisionCG(float *f, const float *f_new, float *U, const float *V, const float *W, float *rho, const float *wf, const int *ex, const int *ey, const int *ez, const bool *wall, const int x, const int y, const int z, const float *tau);

__global__ void Streaming(const float *f, float *f_new, int *ex, int *ey, int *ez, const int x, const int y, const int z);
__global__ void Bound(float *f, float *f_new, float *rho, float *u, float *v, float *w, int *ex, int *ey, int *ez, float *wf, bool *wall, int x, int y, int z, int t);
__global__ void Macro(const float *f_new, float *rho, float *U, float *V, float *W, const int *ex, const int *ey, const int *ez, const bool *wall, const int x, const int y, const int z);

__host__ void WriteFluidVTK(const size_t time, const float *u, const float *v, const float *w, const float *rho, bool *wall);
__host__ void WriteSolidVTK(const bool *wall);

__global__ void SetTau(float *tau)
{
    tau[1] = 0.9;
    tau[0] = 1.;
}

int main()
{
    bool *d_wall, *wall;
    float *d_f, *d_f_new;
    int *d_ex, *d_ey, *d_ez;
    float *U, *V, *W, *rho;
    float *d_U, *d_V, *d_W, *d_rho, *d_psi;
    float *d_wf;
    float *d_Fx, *d_Fy, *d_Fz;

    float *d_tau;

    cudaMalloc(&d_wall, LX * LY * LZ * sizeof(bool));

    wall = new bool[LX * LY * LZ];

    cudaMalloc(&d_tau, no_sub * sizeof(float));

    SetTau<<<1, 1>>>(d_tau);

    cudaMalloc(&d_f, LX * LY * LZ * S * no_sub * sizeof(float));
    cudaMalloc(&d_f_new, LX * LY * LZ * S * no_sub * sizeof(float));

    cudaMalloc(&d_rho, LX * LY * LZ * no_sub * sizeof(float));
    cudaMalloc(&d_psi, LX * LY * LZ * no_sub * sizeof(float));

    U = new float[LX * LY * LZ];
    V = new float[LX * LY * LZ];
    W = new float[LX * LY * LZ];

    rho = new float[LX * LY * LZ * no_sub];

    cudaMalloc(&d_U, LX * LY * LZ * sizeof(float));
    cudaMalloc(&d_V, LX * LY * LZ * sizeof(float));
    cudaMalloc(&d_W, LX * LY * LZ * sizeof(float));

    cudaMalloc(&d_Fx, LX * LY * LZ * no_sub * sizeof(float));
    cudaMalloc(&d_Fy, LX * LY * LZ * no_sub * sizeof(float));
    cudaMalloc(&d_Fz, LX * LY * LZ * no_sub * sizeof(float));

    cudaMalloc(&d_ex, S * sizeof(int));
    cudaMalloc(&d_ey, S * sizeof(int));
    cudaMalloc(&d_ez, S * sizeof(int));

    cudaMalloc(&d_wf, S * sizeof(float));

    int tpbSizex = 32;
    int tpbSizey = 8;
    int tpbSizez = 1;

    int bpgSizex = LX / tpbSizex;
    int bpgSizey = LY / tpbSizey;
    int bpgSizez = LZ / tpbSizez;

    if (tpbSizex % LX)
    {
        bpgSizex += 1;
    }

    if (tpbSizey % LY)
    {
        bpgSizey += 1;
    }

    if (tpbSizez % LZ)
    {
        bpgSizez += 1;
    }

    dim3 blocksPerGrid(bpgSizex, bpgSizey, bpgSizez);
    dim3 threadsPerBlock(tpbSizex, tpbSizey, tpbSizez);

    GeometryFromFile(wall, LX, LY, LZ);

    cudaMemcpy(d_wall, wall, LX * LY * LZ * sizeof(bool), cudaMemcpyHostToDevice);

    WriteSolidVTK(wall);

    RandomDensity(rho, LX, LY, LZ);

    cout << "kul" << endl;

    cudaMemcpy(d_rho, rho, LX * LY * LZ * no_sub * sizeof(float), cudaMemcpyHostToDevice);

    Init<<<blocksPerGrid, threadsPerBlock>>>(d_f, d_f_new, d_rho, d_U, d_V, d_W, d_Fx, d_Fy, d_Fz, d_wf, d_ex, d_ey, d_ez, d_wall, LX, LY, LZ);

    for (int t = 0; t < tmax; ++t)
    {
        CollisionCG<<<blocksPerGrid, threadsPerBlock>>>(d_f, d_f_new, d_U, d_V, d_W, d_rho, d_wf, d_ex, d_ey, d_ez, d_wall, LX, LY, LZ, d_tau);
        
        if (t % pfeq == 0)
        {
            cudaMemcpy(U, d_U, LX * LY * LZ * sizeof(float), cudaMemcpyDeviceToHost);
            cudaMemcpy(V, d_V, LX * LY * LZ * sizeof(float), cudaMemcpyDeviceToHost);
            cudaMemcpy(W, d_W, LX * LY * LZ * sizeof(float), cudaMemcpyDeviceToHost);
            cudaMemcpy(rho, d_rho, LX * LY * LZ * no_sub * sizeof(float), cudaMemcpyDeviceToHost);
            WriteFluidVTK(t, U, V, W, rho, wall);

            auto now = chrono::system_clock::now();
            time_t nowt = chrono::system_clock::to_time_t(now);

            cout << t << " " << rho[LX / 2 + LX * LY / 2 + LX * LY * LZ / 3 + LX * LY * LZ] << " New file at " << ctime(&nowt);
        }

        else if (!(t % 1000)) {
            auto now = chrono::system_clock::now();
            time_t nowt = chrono::system_clock::to_time_t(now);

            cout << t << " at " << ctime(&nowt);
        }

        Streaming<<<blocksPerGrid, threadsPerBlock>>>(d_f, d_f_new, d_ex, d_ey, d_ez, LX, LY, LZ);
        // Bound<<<blocksPerGrid, threadsPerBlock>>>(d_f, d_f_new, d_rho, d_U, d_V, d_W, d_ex, d_ey, d_ez, d_wf, d_wall, LX, LY, LZ, t);
        Macro<<<blocksPerGrid, threadsPerBlock>>>(d_f_new, d_rho, d_U, d_V, d_W, d_ex, d_ey, d_ez, d_wall, LX, LY, LZ);
    }

    cudaFree(d_wall);
    cudaFree(d_f);
    cudaFree(d_f_new);
    cudaFree(d_U);
    cudaFree(d_V);
    cudaFree(d_W);
    cudaFree(d_rho);
    cudaFree(d_ex);
    cudaFree(d_ey);
    cudaFree(d_ez);
    cudaFree(d_wf);

    return 0;
}

__host__ void GeometryFromFile(bool *ActWall, const int x, const int y, const int z)
{
    for (int k = 0; k < z; ++k)
    {
        for (int j = 0; j < y; ++j)
        {
            for (int i = 0; i < x; ++i)
            {
                ActWall[i + x * j + x * y * k] = false;
            }
        }
    }
}

__host__ void RandomDensity(float *rho, const int x, const int y, const int z)
{
    for (int k = 0; k < z; ++k)
    {
        for (int j = 0; j < y; ++j)
        {
            for (int i = 0; i < x; ++i)
            {
                rho[i + x * j + x * y * k + x * y * z * 0] = 0.;
                rho[i + x * j + x * y * k + x * y * z * 1] = 1.2;

                int I = (0.5 * x - i);
                int J = (0.5 * y - j);
                int K = (0.5 * z - k);
                
                int R = 0.15 * x;


                if (I * I + J * J + K * K < R * R) {
                    rho[i + x * j + x * y * k + x * y * z * 0] = 1.;
                    rho[i + x * j + x * y * k + x * y * z * 1] = 0.;
                }
            }
        }
    }
}

__global__ void Init(float *f, float *f_new, float *rho, float *U, float *V, float *W, float *Fx, float *Fy, float *Fz, float *wf, int *ex, int *ey, int *ez, const bool *wall, const int x, const int y, const int z)
{
    ex[0]  =  0;   ey[0]  =  0;   ez[0]  =  0;   wf[0]  = 1. / 3.;
    ex[1]  =  1;   ey[1]  =  0;   ez[1]  =  0;   wf[1]  = 1. / 18.;
    ex[2]  = -1;   ey[2]  =  0;   ez[2]  =  0;   wf[2]  = 1. / 18.;
    ex[3]  =  0;   ey[3]  =  1;   ez[3]  =  0;   wf[3]  = 1. / 18.;
    ex[4]  =  0;   ey[4]  = -1;   ez[4]  =  0;   wf[4]  = 1. / 18.;
    ex[5]  =  0;   ey[5]  =  0;   ez[5]  =  1;   wf[5]  = 1. / 18.;
    ex[6]  =  0;   ey[6]  =  0;   ez[6]  = -1;   wf[6]  = 1. / 18.;
    ex[7]  =  1;   ey[7]  =  1;   ez[7]  =  0;   wf[7]  = 1. / 36.;
    ex[8]  = -1;   ey[8]  = -1;   ez[8]  =  0;   wf[8]  = 1. / 36.;
    ex[9]  =  1;   ey[9]  =  0;   ez[9]  =  1;   wf[9]  = 1. / 36.;
    ex[10] = -1;   ey[10] =  0;   ez[10] = -1;   wf[10] = 1. / 36.;
    ex[11] =  0;   ey[11] =  1;   ez[11] =  1;   wf[11] = 1. / 36.;
    ex[12] =  0;   ey[12] = -1;   ez[12] = -1;   wf[12] = 1. / 36.;
    ex[13] =  1;   ey[13] = -1;   ez[13] =  0;   wf[13] = 1. / 36.;
    ex[14] = -1;   ey[14] =  1;   ez[14] =  0;   wf[14] = 1. / 36.;
    ex[15] =  1;   ey[15] =  0;   ez[15] = -1;   wf[15] = 1. / 36.;
    ex[16] = -1;   ey[16] =  0;   ez[16] =  1;   wf[16] = 1. / 36.;
    ex[17] =  0;   ey[17] =  1;   ez[17] = -1;   wf[17] = 1. / 36.;
    ex[18] =  0;   ey[18] = -1;   ez[18] =  1;   wf[18] = 1. / 36.;

    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;
    int k = threadIdx.z + blockDim.z * blockIdx.z;

    if (i < x && j < y && k < z)
    {
        for (int sub = 0; sub < no_sub; ++sub)
        {
            const int coor = i + x * j + x * y * k + x * y * z * sub;

            Fx[coor] = 0.;
            Fy[coor] = 0.;
            Fz[coor] = 0.;

            if (wall[i + x * j + x * y * k])
            {
                if (sub)
                {
                    rho[coor] = 0.;
                }

                else
                {
                    rho[coor] = 0.;
                }
            }

            for (int a = 0; a < S; a++)
            {
                const int id = i + x * j + x * y * k + x * y * z * a + x * y * z * S * sub;

                f[id] = wf[a] * rho[coor];
                f_new[id] = f[id];
            }

            if (wall[i + x * j + x * y * k])
            {
                if (sub)
                {
                    rho[coor] = 0.;
                }

                else
                {
                    rho[coor] = 0.15;
                }

                if (i > x - 10)
                {
                    if (sub)
                    {
                        rho[coor] = 0.;
                    }

                    else
                    {
                        rho[coor] = 0.;
                    }
                }
            }
        }

        const int coor = i + x * j + x * y * k;

        U[coor] = 0.;
        V[coor] = 0.;
        W[coor] = 0.;
    }
}

__global__ void CollisionCG(float *f, const float *f_new, float *U, const float *V, const float *W, float *rho, const float *wf, const int *ex, const int *ey, const int *ez, const bool *wall, const int x, const int y, const int z, const float *tau)
{
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;
    int k = threadIdx.z + blockDim.z * blockIdx.z;

    if (i < x && j < y && k < z)
    {
        const int coor = i + x * j + x * y * k;

        if (!wall[coor])
        {
            float A[no_sub] = {0.1, 0.1};
            float beta = 0.5;
            float omega = 1.;

            float ak[no_sub] = {0.1, 0.2};

            float delta = 0.98;
            float psi = (rho[coor + x * y * z * 0] - rho[coor + x * y * z * 1]) / (rho[coor + x * y * z * 0] + rho[coor + x * y * z * 1]);

            if (psi > delta)
            {
                omega = 1. / tau[0];
            }

            else if (psi <= delta && psi > 0.)
            {
                float s1 = 2. * tau[0] * tau[1] / (tau[0] + tau[1]);
                float s2 = 2. * (tau[0] - s1) / delta;
                float s3 = -s2 / (2. * delta);

                omega = s1 + s2 * psi + s3 * psi * psi;
                omega = 1. / omega;
            }

            else if (psi >= -delta && psi <= 0.)
            {
                float t1 = 2. * tau[0] * tau[1] / (tau[0] + tau[1]);
                float t2 = 2. * (t1 - tau[1]) / delta;
                float t3 = t2 / (2. * delta);

                omega = t1 + t2 * psi + t3 * psi * psi;
                omega = 1. / omega;
            }

            else
            {
                omega = 1. / tau[1];
            }

            float Om1[no_sub][S], Om2[no_sub][S];

            Om2[0][0] = 0.;
            Om2[1][0] = 0.;
            
            float Fx = 0.; // color gradient x
            float Fy = 0.; // color gradient y
            float Fz = 0.; // color gradient z

            for (int a = 0; a < S; ++a)
            {
                int i_d = (i + ex[a] + x) % x;
                int j_d = (j + ey[a] + y) % y;
                int k_d = (k + ez[a] + z) % z;

                int coor_mod = i_d + x * j_d + x * y * k_d;

                float pf = (rho[coor_mod + x * y * z * 0] - rho[coor_mod + x * y * z * 1]) / (rho[coor_mod + x * y * z * 0] + rho[coor_mod + x * y * z * 1]);
                
                Fx += ex[a] * pf;
                Fy += ey[a] * pf;
                Fz += ez[a] * pf;
            }

            float F_sq = Fx * Fx + Fy * Fy + Fz * Fz;
            float F_sz = sqrtf(F_sq); // gradient magnitude

            const float ueq = U[coor];
            const float veq = V[coor];
            const float weq = W[coor];

            float u_sq = ueq * ueq + veq * veq + weq * weq;

            if (F_sz > 1e-10)
            {
                float B[S];

                B[0] = -2. / 9.;
                B[1] = 1. / 18.;   B[2] = B[1];   B[3] = B[1];   B[4] = B[1];   B[5] = B[1];   B[6] = B[1];
                B[7] = 0.;  B[8] = B[7];   B[9] = B[7];   B[10] = B[7];  B[11] = B[7];  B[12] = B[7]; B[13] = B[7];  B[14] = B[7];  B[15] = B[7];  B[16] = B[7];  B[17] = B[7]; B[18] = B[7];

                for (int sub = 0; sub < no_sub; ++sub)
                {
                    float rh = rho[coor + x * y * z * sub];

                    for (int a = 0; a < S; ++a)
                    {
                        float wf_q; // alpha|phi Eq. (2.127)

                        if (a < 1)
                        {
                            wf_q = ak[sub];
                        }

                        else if (a < 7)
                        {
                            wf_q = (1. - ak[sub]) / 12.;
                        }

                        else
                        {
                            wf_q = (1. - ak[sub]) / 24.;
                        }

                        float u_vec = ex[a] * ueq + ey[a] * veq + ez[a] * weq;

                        float feq = rh * (wf_q + wf[a] * (3. * u_vec + 4.5 * u_vec * u_vec - 1.5 * u_sq));

                        Om1[sub][a] = omega * (feq - f_new[coor + x * y * z * a + x * y * z * S * sub]); // regular collision

                        if (a)
                        {
                            float eF = ex[a] * Fx + ey[a] * Fy + ez[a] * Fz;

                            Om2[sub][a] = 0.5 * A[sub] * F_sz * (wf[a] * eF * eF / F_sq - B[a]); // Eq. (2.131)
                        }
                    }
                }

                float rh_s = rho[coor + x * y * z * 0] + rho[coor + x * y * z * 1];

                for (int a = 0; a < S; ++a)
                {
                    float f0 = f_new[coor + x * y * z * a + x * y * z * S * 0] + Om1[0][a] + Om2[0][a];

                    float f1 = f_new[coor + x * y * z * a + x * y * z * S * 1] + Om1[1][a] + Om2[1][a];

                    float f_sum = f0 + f1;

                    f[coor + x * y * z * a + x * y * z * S * 0] = rho[coor + x * y * z * 0] / rh_s * f_sum; // 1st part of Eq. (2.134)

                    f[coor + x * y * z * a + x * y * z * S * 1] = rho[coor + x * y * z * 1] / rh_s * f_sum; // 1st part of Eq. (2.135)

                    if (a)
                    {
                        float eF = ex[a] * Fx + ey[a] * Fy + ez[a] * Fz; // color gradient from Eq. (2.136)
                        float e_s = sqrtf(ex[a] * ex[a] + ey[a] * ey[a] + ez[a] * ez[a]); // e magnitude
                        
                        for (int sub = 0; sub < no_sub; ++sub)
                        {
                            float wf_q; // Eq. (2.127)

                            else if (a < 7)
                            {
                                wf_q = (1. - ak[sub]) / 12.;
                            }

                            else
                            {
                                wf_q = (1. - ak[sub]) / 24.;
                            }

                            f[coor + x * y * z * a + x * y * z * S * sub] += powf(-1., sub) * beta * rho[coor + x * y * z * 0] * rho[coor + x * y * z * 1] / (rh_s * rh_s) * wf_q * rh_s * eF / (e_s * F_sz); // wf_q * rh_s == equilibrium (u=0)
                        }
                    }
                }
            }
            
            else
            {
                for (int sub = 0; sub < no_sub; ++sub)
                {
                    float rh = rho[coor + x * y * z * sub];

                    for (int a = 0; a < S; ++a)
                    {
                        float wf_q;

                        if (a < 1)
                        {
                            wf_q = ak[sub];
                        }

                        else if (a < 7)
                        {
                            wf_q = (1. - ak[sub]) / 12.;
                        }

                        else
                        {
                            wf_q = (1. - ak[sub]) / 24.;
                        }

                        float u_vec = ex[a] * ueq + ey[a] * veq + ez[a] * weq;

                        float feq = rh * (wf_q + wf[a] * (3. * u_vec + 4.5 * u_vec * u_vec - 1.5 * u_sq));

                        Om1[sub][a] = omega * (feq - f_new[coor + x * y * z * a + x * y * z * S * sub]);
                    }
                }

                for (int a = 0; a < S; ++a)
                {
                    f[coor + x * y * z * a + x * y * z * S * 0] = f_new[coor + x * y * z * a + x * y * z * S * 0] + Om1[0][a];

                    f[coor + x * y * z * a + x * y * z * S * 1] = f_new[coor + x * y * z * a + x * y * z * S * 1] + Om1[1][a];
                }
            }
        }
    }
}

__global__ void Streaming(const float *f, float *f_new, int *ex, int *ey, int *ez, const int x, const int y, const int z)
{
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;
    int k = threadIdx.z + blockDim.z * blockIdx.z;

    if (i < x && j < y && k < z)
    {
        for (int sub = 0; sub < no_sub; ++sub)
        {
            const int sub_add = x * y * z * S * sub;
            
            for (int a = 0; a < S; ++a)
            {
                int i_mod = (i + ex[a] + x) % x;
                int j_mod = (j + ey[a] + y) % y;
                int k_mod = (k + ez[a] + z) % z;

                int coor     = i     + x * j     + x * y * k     + x * y * z * a + sub_add;
                int coor_mod = i_mod + x * j_mod + x * y * k_mod + x * y * z * a + sub_add;

                f_new[coor_mod] = f[coor];
            }
        }
    }
}

__global__ void Bound(float *f, float *f_new, float *rho, float *u, float *v, float *w, int *ex, int *ey, int *ez, float *wf, bool *wall, int x, int y, int z, int t)
{
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;
    int k = threadIdx.z + blockDim.z * blockIdx.z;

    if (i < x && j < y && k < z)
    {
        int coor = i + x * j + x * y * k;

        if (wall[coor])
        {
            int anti[S] = {0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15, 18, 17};

            for (int a = 0; a < S; ++a)
            {
                int i_mod = (i + ex[a] + x) % x;
                int j_mod = (j + ey[a] + y) % y;
                int k_mod = (k + ez[a] + z) % z;

                int coor_mod = i_mod + x * j_mod + x * y * k_mod;

                if (!wall[coor_mod])
                {
                    for (int sub = 0; sub < no_sub; ++sub)
                    {
                        int sub_add = x * y * z * S * sub;

                        f[coor + x * y * z * a + sub_add] = f_new[coor + x * y * z * anti[a] + sub_add];
                    }
                }
            }
        }

        else
        {
            if (i == 1)
            {
                //INLET______________________________
                //___________________________________

                float rh_sum = 1.;
                float rh0 = 0.;
                float rh1 = 0.;

                if (!((t / 1000) % 2))
                {
                    rh1 = 1.1;
                }

                else
                {
                    rh0 = 1.1;
                }

                float rh_part[no_sub] = {rh0, rh1};
                
                int in[5]  = {1, 7,  9, 13, 15};

                float ueq = u[coor + 1];
                float veq = v[coor + 1];
                float weq = w[coor + 1];
                
                float rh = rh0 + rh1;
                float rhn = rho[coor + x * y * z * 0 + 1] + rho[coor + x * y * z * 1 + 1];//0.;
                
                float usq = ueq * ueq;
                float vsq = veq * veq;
                float wsq = weq * weq;

                float u_sq = usq + vsq + wsq;

                float feq0[5], feqn[5];

                for (int a = 0; a < 5; ++a)
                {
                    float u_vec = ex[in[a]] * ueq + ey[in[a]] * veq + ez[in[a]] * weq;

                    feq0[a] = wf[in[a]] * rh * (1. + 3. * u_vec + 4.5 * u_vec * u_vec - 1.5 * u_sq);

                    feqn[a] = wf[in[a]] * rhn * (1. + 3. * u_vec + 4.5 * u_vec * u_vec - 1.5 * u_sq);
                }
                
                float F_new[S];

                for (int a = 0; a < S; ++a)
                {
                    F_new[a] = 0.;

                    for (int sub = 0; sub < no_sub; ++sub)
                    {
                        const int sub_add = x * y * z * S * sub;

                        F_new[a] += f_new[coor + x * y * z * a + sub_add + 1];
                    }
                }
                
                for (int a = 0; a < 5; ++a)
                {
                    for (int sub = 0; sub < no_sub; ++sub)
                    {
                        const int sub_add = x * y * z * S * sub;

                        f_new[coor + x * y * z * in[a] + sub_add] = (feq0[a] + (F_new[in[a]] - feqn[a])) * rh_part[sub] / rh_sum;
                    }
                }
            }

            if (i == x - 2)
            {
                float rh_sum = 0.;
                float rh_part[no_sub];
                
                for (int sub = 0; sub < no_sub; ++sub)
                {
                    int sub_add = x * y * z * sub;

                    rh_part[sub] = rho[coor + sub_add];
                    
                    if (rh_part[sub] < 0.)
                    {
                        rh_part[sub] = 0.;
                    }

                    rh_sum += rh_part[sub];
                }
                
                int out[5] = {2, 8, 10, 14, 16};

                float ueq = 0.005;
                float veq = 0.;
                float weq = 0.;
                
                float rhn = 0.;

                for (int sub = 0; sub < no_sub; ++sub)
                {
                    const int sub_add = x * y * z * sub;
                    rhn += rho[coor + sub_add - 1];
                }
                
                float usq = ueq * ueq;
                float vsq = veq * veq;
                float wsq = weq * weq;

                float u_sq = usq + vsq + wsq;

                float feq0[5], feqn[5];

                for (int a = 0; a < 5; ++a)
                {
                    float u_vec = ex[out[a]] * ueq + ey[out[a]] * veq + ez[out[a]] * weq;

                    feq0[a] = wf[out[a]] * rhn * (1. + 3. * u_vec + 4.5 * u_vec * u_vec - 1.5 * u_sq);
                }

                ueq = 2. * u[coor - 1] - u[coor - 2];
                veq = 0.;
                weq = 0.;
                
                usq = ueq * ueq;
                vsq = veq * veq;
                wsq = weq * weq;

                u_sq = usq + vsq + wsq;

                for (int a = 0; a < 5; ++a)
                {
                    float u_vec = ex[out[a]] * ueq + ey[out[a]] * veq + ez[out[a]] * weq;

                    feqn[a] = wf[out[a]] * rhn * (1. + 3. * u_vec + 4.5 * u_vec * u_vec - 1.5 * u_sq);
                }
                
                float F_new[S];

                for (int a = 0; a < S; ++a)
                {
                    F_new[a] = 0.;

                    for (int sub = 0; sub < no_sub; ++sub)
                    {
                        const int sub_add = x * y * z * S * sub;

                        F_new[a] += f_new[coor + x * y * z * a + sub_add - 1];
                    }
                }
                
                for (int a = 0; a < 5; ++a)
                {
                    for (int sub = 0; sub < no_sub; ++sub)
                    {
                        const int sub_add = x * y * z * S * sub;

                        f_new[coor + x * y * z * out[a] + sub_add] = (feq0[a] + (F_new[out[a]] - feqn[a])) * rh_part[sub] / rh_sum;
                    }
                }
            }
        }
    }
}

__global__ void Macro(const float *f_new, float *rho, float *U, float *V, float *W, const int *ex, const int *ey, const int *ez, const bool *wall, const int x, const int y, const int z)
{
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;
    int k = threadIdx.z + blockDim.z * blockIdx.z;

    if (i < x && j < y && k < z)
    {
        if (!wall[i + x * j + x * y * k])
        {
            float u_sum = 0.;
            float v_sum = 0.;
            float w_sum = 0.;

            for (int sub = 0; sub < no_sub; ++sub)
            {
                const int coor = i + x * j + x * y * k + x * y * z * sub;

                if (!wall[i + x * j + x * y * k])
                {
                    rho[coor] = 0.;

                    for (int a = 0; a < S; ++a)
                    {
                        const float ft = f_new[i + x * j + x * y * k + x * y * z * a + x * y * z * S * sub];

                        rho[coor] += ft;

                        u_sum += ex[a] * ft;
                        v_sum += ey[a] * ft;
                        w_sum += ez[a] * ft;
                    }

                    if (rho[coor] < 0.)
                    {
                        rho[coor] = 0.;
                    }
                }
            }

            const int coor = i + x * j + x * y * k;

            U[coor] = u_sum / (rho[coor + x * y * z * 0] + rho[coor + x * y * z * 1]);
            V[coor] = v_sum / (rho[coor + x * y * z * 0] + rho[coor + x * y * z * 1]);
            W[coor] = w_sum / (rho[coor + x * y * z * 0] + rho[coor + x * y * z * 1]);
        }
    }
}

//print
__host__ void WriteFluidVTK(const size_t time, const float *u, const float *v, const float *w, const float *rho, bool *wall)
{
    /// Create filename

    if (system("mkdir -p vtk_fluid"))
    {
        cout << "Could not create /vtk_fluid.\n";
        return;
    }

    stringstream output_filename;
    output_filename << "vtk_fluid/fluid_t_CG_pol_n_nekaj_A_0_05_u_0_005_5050_big_half_old_geo_kontra_" << time << ".vtk";
    ofstream output_file;

    /// Open file

    output_file.open(output_filename.str().c_str());

    /// Write VTK header

    output_file << "# vtk DataFile Version 3.0\n";
    output_file << "fluid_state\n";
    output_file << "ASCII\n";
    output_file << "DATASET RECTILINEAR_GRID\n";
    output_file << "DIMENSIONS " << LX / fact << " " << LY / fact << " " << LZ / fact << "\n";
    output_file << "X_COORDINATES " << LX / fact << " float\n";

    for (int i = 0; i < LX; i += fact)
    {
        output_file << i << " ";
    }

    output_file << "\n";
    output_file << "Y_COORDINATES " << LY / fact << " float\n";

    for (int j = 0; j < LY; j += fact)
    {
        output_file << j << " ";
    }

    output_file << "\n";
    output_file << "Z_COORDINATES " << LZ / fact << " float\n";

    for (int k = 0; k < LZ; k += fact)
    {
        output_file << k << " ";
    }

    output_file << "\n";

    output_file << "POINT_DATA " << LX * LY * LZ / (fact * fact * fact) << "\n";

    /// Write density difference


    for (int sub = 0; sub < no_sub; ++sub)
    {
        output_file << "SCALARS density" << sub << " float 1\n";
        output_file << "LOOKUP_TABLE default\n";

        for (int k = 0; k < LZ; k += fact)
        {
            for (int j = 0; j < LY; j += fact)
            {
                for (int i = 0; i < LX; i += fact)
                {
                    int coor = i + LX * j + LX * LY * k;

                    if (!wall[coor])
                        output_file << rho[coor + LX * LY * LZ * sub] << "\n";
                    else
                        output_file << 0 << "\n";
                }
            }
        }
    }

    /// Write velocity

    output_file << "VECTORS velocity_vector float\n";

    for (int k = 0; k < LZ; k += fact)
    {
        for (int j = 0; j < LY; j += fact)
        {
            for (int i = 0; i < LX; i += fact)
            {
                int coor = i + LX * j + LX * LY * k;
                output_file << u[coor] << " " << v[coor] << " " << w[coor] << " \n";
            }
        }
    }

    /// Close file

    output_file.close();

    return;
}

__host__ void WriteSolidVTK(const bool *wall)
{
    /// Create filename

    if (system("mkdir -p vtk_solid"))
    {
        cout << "Could not create /vtk_solid.\n";
        return;
    }

    stringstream output_filename;
    output_filename << "vtk_solid/solid_pol_n_nekaj_CG_big_half_old_geo_kontra.vtk";

    ofstream output_file;

    /// Open file

    output_file.open(output_filename.str().c_str());

    /// Write VTK header

    output_file << "# vtk DataFile Version 3.0\n";
    output_file << "fluid_state\n";
    output_file << "ASCII\n";
    output_file << "DATASET RECTILINEAR_GRID\n";
    output_file << "DIMENSIONS " << LX / fact << " " << LY / fact << " " << LZ / fact << "\n";
    output_file << "X_COORDINATES " << LX / fact << " float\n";

    for (int i = 0; i < LX; i += fact)
    {
        output_file << i << " ";
    }

    output_file << "\n";
    output_file << "Y_COORDINATES " << LY / fact << " float\n";

    for (int j = 0; j < LY; j += fact)
    {
        output_file << j << " ";
    }

    output_file << "\n";
    output_file << "Z_COORDINATES " << LZ / fact << " float\n";

    for (int k = 0; k < LZ; k += fact)
    {
        output_file << k << " ";
    }

    output_file << "\n";

    output_file << "POINT_DATA " << LX * LY * LZ / (fact * fact * fact) << "\n";

    /// Write wall
    output_file << "SCALARS wall float 1\n";
    output_file << "LOOKUP_TABLE default\n";

    for (int k = 0; k < LZ; k += fact)
    {
        for (int j = 0; j < LY; j += fact)
        {
            for (int i = 0; i < LX; i += fact)
            {
                int coor = i + LX * j + LX * LY * k;
                  output_file << static_cast<int>(wall[coor]) << "\n";
            }
        }
    }

    /// Close file

    output_file.close();

    return;
}
